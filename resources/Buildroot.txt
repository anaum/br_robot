*** Settings ***
Library           Operating System

*** Variables ***
${BR_OUT_DIR}     ${CURDIR}/../sut/output
${BR_DIR}         ${CURDIR}/../../buildroot

*** Keywords ***
Create Defconfig
    [Arguments]    ${defconfig}
    Create Directory    ${BR_OUT_DIR}
    Copy File    ${CURDIR}/../test_accessories/${defconfig}    ${BR_OUT_DIR}/.config

Append To Defconfig
    [Arguments]    ${defconfig_to_append}
    ${content_to_append}=    Get File    ${CURDIR}/../test_accessories/${defconfig_to_append}
    Append To File    ${BR_OUT_DIR}/.config    ${content_to_append}

Load Defconfig
    ${rc}=    Run And Return Rc    make -C ${BR_DIR} O=${BR_OUT_DIR} olddefconfig > ${resultdir}/build.log 2>&1
    Should Be Equal As Integers    ${rc}    0    Cannot olddefconfig

Build
    ${rc}=    Run And Return Rc    make -C ${BR_OUT_DIR} >> ${resultdir}/build.log 2>&1
    Should Be Equal As Integers    ${rc}    0    Build failed
