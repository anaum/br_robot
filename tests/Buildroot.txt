*** Variables ***
${BR_OUT_DIR}     ${CURDIR}/../sut/output
${BR_DIR}         ${CURDIR}/../../buildroot

*** Keywords ***
Load Defconfig
    Create Directory    ${BR_OUT_DIR}
    Copy File    ../test_accessories/basic_toolchain_config    ${BR_OUT_DIR}/.config
    ${rc}=    Run And Return Rc    make -C ${BR_DIR} O=${BR_OUT_DIR} olddefconfig > ${resultdir}/build.log 2>&1
    Should Be Equal As Integers    ${rc}    0    Cannot olddefconfig

Build
    ${rc}=    Run And Return Rc    make -C ${BR_OUT_DIR} >> ${resultdir}/build.log 2>&1
    Should Be Equal As Integers    ${rc}    0    Build failed
